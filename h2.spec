Name:                h2
Version:             1.4.196
Release:             5
Summary:             Java SQL database
License:             (EPL-1.0 or MPLv2.0 or LGPLv3+) and ASL 2.0
URL:                 http://www.h2database.com
Source0:             http://www.h2database.com/h2-2017-06-10.zip
Source1:             https://repo1.maven.org/maven2/com/h2database/h2/%{version}/h2-%{version}.pom
Patch0:              port-to-lucene-6.patch
BuildArch:           noarch
BuildRequires:       javapackages-local lucene >= 6.1.0 lucene-analysis >= 6.1.0
BuildRequires:       lucene-queryparser >= 6.1.0 slf4j felix-osgi-core glassfish-servlet-api <= 3.1.0 jts
%description
H2 is a the Java SQL database. The main features of H2 are: Very fast, open
source, JDBC API; Embedded and server modes; In-memory databases; Browser
based Console application; Small footprint: around 1 MB jar file size.

%package javadoc
Summary:             Javadocs for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n %{name}
%patch 0 -p2
rm src/main/org/h2/util/OsgiDataSourceFactory.java
sed -i -e '/OsgiDataSourceFactory/d' src/main/org/h2/util/DbDriverActivator.java
sed -i -e '/org.osgi.service.jdbc/d' src/main/META-INF/MANIFEST.MF
find -name '*.class' -delete
find -name '*.jar' -delete
find -name '*.exe' -delete
find -name '*.dll' -delete
sed -i -e 's/com\.vividsolutions\.jts/org.locationtech.jts/' $(find -name *.java)
sed -i -e 's/vividsolutions/vividsolutions locationtech/' src/tools/org/h2/build/doc/dictionary.txt
sed -i -e '/downloadTest();/d' -e '/download();/d' \
  src/tools/org/h2/build/Build.java
sed -i -e '/TestNetUtils/d' \
  src/test/org/h2/test/TestAll.java
mkdir ext
ln -s -T $(build-classpath jts) ext/jts-core-1.14.0.jar
ln -s -T $(build-classpath glassfish-servlet-api) ext/servlet-api-3.1.0.jar
ln -s -T $(build-classpath slf4j/api) ext/slf4j-api-1.6.0.jar
ln -s -T $(build-classpath slf4j/nop) ext/slf4j-nop-1.6.0.jar
ln -s -T $(build-classpath lucene/lucene-core) ext/lucene-core-6.1.0.jar
ln -s -T $(build-classpath lucene/lucene-analyzers-common) ext/lucene-analyzers-common-6.1.0.jar
ln -s -T $(build-classpath lucene/lucene-queryparser) ext/lucene-queryparser-6.1.0.jar
ln -s -T $(build-classpath felix/org.osgi.core) ext/org.osgi.core-4.2.0.jar
echo "classic queryparser" >> src/tools/org/h2/build/doc/dictionary.txt

%build
export JAVA_HOME=%{_jvmdir}/java
sh build.sh jar docs

%install
%mvn_artifact %SOURCE1 bin/h2-%{version}.jar
%mvn_install -J docs/javadoc

%files -f .mfiles
%doc docs/index.html
%doc docs/html
%license src/docsrc/html/license.html

%files javadoc -f .mfiles-javadoc
%license src/docsrc/html/license.html

%changelog
* Mon Dec 16 2024 litian <dev11105@linx-info.com> - 1.4.196-5
- fix %patchN is deprecated warning

* Fri Sep 23 2022 caodongxia <caodongxia@h-partners.com> - 1.4.196-4
- Add glassfish-servlet-api version limit

* Mon Jun 27 2022 Xinyi Gou <plerks@163.com> - 1.4.196-3
- Specify license version

* Fri Dec 25 2020 wutao <wutao61@huawei.com> - 1.4.196-2
- Turn off testFast to solve probabilistic failures

* Mon Aug 3 2020 yanan li <liyanan032@huawei.com> - 1.4.196-1
- Package init
